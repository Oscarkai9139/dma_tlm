#ifndef DMA_H
#define DMA_H

#include "register_define.h"

#include "systemc.h"
#include "tlm.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/simple_initiator_socket.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

SC_MODULE(DMA){
  sc_in<bool> clk;
  sc_in<bool> rst;
  sc_out<bool> ir;

  tlm_utils::simple_target_socket<DMA> slave_port;
  tlm_utils::simple_initiator_socket<DMA> master_port;
  unsigned char data_m;
  unsigned char* reg_m;
  bool          rw_m;

  // Control registers, externally addressable, addressed as:
  // 0x0  R_SOURCE
  // 0x4  R_TARGET
  // 0x8  R_SIZE
  // 0xc  R_START
  sc_uint<32> R_SOURCE,
              R_TARGET,
              R_SIZE;
  bool        R_START;

  // Internal registers, not externally addressable
  sc_uint<32> DMA_BASE_ADDR;
  sc_uint<32> t_addr_temp;
  sc_uint<32> s_addr_temp;

  void dma();
  void b_transport(tlm::tlm_generic_payload&, sc_time&);

  SC_CTOR(DMA) : slave_port("slave_port"), master_port("master_port") {
    slave_port.register_b_transport(this, &DMA::b_transport);

    SC_CTHREAD(dma, clk.pos());
    reset_signal_is(rst,true);

    DMA_BASE_ADDR = 0xA0000000;
  }
};

#endif
