#include "dma.h"

void DMA::b_transport(tlm::tlm_generic_payload& trans, sc_time& delay){

  tlm::tlm_command cmd_s  = trans.get_command();
  sc_dt::uint64    addr_s = trans.get_address();
  unsigned char*   data_s = trans.get_data_ptr();
  unsigned int     len    = trans.get_data_length();

  if (cmd_s == tlm::TLM_WRITE_COMMAND) {
    cout << "DMA WRITE CONTROL REGs" << endl;
    //DMA Initialization from cpu
    if(!R_START){
      if     (addr_s == SOURCE_ADDR) R_SOURCE = *(reinterpret_cast<unsigned char*>(data_s));
      else if(addr_s == TARGET_ADDR) R_TARGET = *(reinterpret_cast<unsigned char*>(data_s));
      else if(addr_s == SIZE_ADDR)   R_SIZE   = *(reinterpret_cast<unsigned char*>(data_s));
      else if(addr_s == START_ADDR)  R_START  = *(reinterpret_cast<unsigned char*>(data_s));
    }
    //DMA read enable
    else {
      rw_m = READ;
    }
  }

  wait(delay);       //use external delay
  //wait(10, SC_NS); //use internal delay
  trans.set_response_status(tlm::TLM_OK_RESPONSE);
}

void DMA::dma() {
  R_SOURCE = 0;
  R_TARGET = 0;
  R_SIZE   = 0;
  R_START  = 0;
  data_m   = 0;

  while (1) {
    wait();

    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
    tlm::tlm_command cmd;
    sc_time delay = sc_time(10, SC_NS);

    while(R_START == 1 && R_SIZE != 0){

      t_addr_temp = R_TARGET;
      s_addr_temp = R_SOURCE;
      //DMA read from source
      if(R_START && rw_m == READ){
        cmd = tlm::TLM_READ_COMMAND;
        trans->set_command(cmd);
        trans->set_address(s_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);

        reg_m = trans->get_data_ptr();
        //cout << "dma read:" << (unsigned int)data_m;
        data_m = *reg_m;
        //cout << "|" << (unsigned int)data_m << endl;
        R_SOURCE = R_SOURCE + 4;
        rw_m = WRITE;
        break;
      }
      //DMA write to target
      else if(R_START && rw_m == WRITE){
        cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(t_addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_m));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);

        R_TARGET = R_TARGET + 4;
        rw_m = READ;
        R_SIZE--;
        break;
      }

    }//close while(R_START == 1 && R_SIZE != 0)

    if(R_SIZE == 0 && R_START == 1){
      R_START = 0;
      ir.write(1);
    }

  }//close while(1)



}//close dma()
