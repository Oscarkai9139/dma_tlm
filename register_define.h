//DMA control register addr
#define SOURCE_ADDR 0x0
#define TARGET_ADDR 0x4
#define SIZE_ADDR   0x8
#define START_ADDR  0xc

//define register values to write
#define source_init 1
#define target_init 100
#define size_init   5

//Port Read write enable
#define READ  0
#define WRITE 1

//DMA start register
#define DMA_START 1
#define DMA_STOP  0

//define ram
#define MEM_SIZE 300
#define MEM_ENABLE 1
