#ifndef CPU_H
#define CPU_H

#include "systemc.h"
#include "tlm.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/simple_initiator_socket.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

SC_MODULE(CPU){

  sc_in<bool> clk;
  sc_in<bool> rst;

  tlm_utils::simple_initiator_socket<CPU> master_port;

  void cpu();
  int initial_setup;

  uint addr_temp;
  uint data_temp;

  SC_CTOR(CPU) : master_port("master_port") {
    SC_CTHREAD(cpu, clk.pos());
    reset_signal_is(rst, true);
  }

};

#endif
