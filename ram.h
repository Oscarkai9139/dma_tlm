#ifndef RAM_H
#define RAM_H

#include "register_define.h"
#include "systemc.h"
#include "tlm.h"
#include "tlm_utils/simple_target_socket.h"
#include "tlm_utils/simple_initiator_socket.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

SC_MODULE(RAM){

  sc_in<bool> clk;
  sc_in<bool> rst;

  tlm_utils::simple_target_socket<RAM> slave_port;

  void ram();
  void b_transport(tlm::tlm_generic_payload&, sc_time&);

  sc_uint<32> RMEM[MEM_SIZE];
  sc_uint<32> data_out;

  SC_CTOR(RAM) : slave_port("slave_port") {

    slave_port.register_b_transport(this, &RAM::b_transport);

    SC_CTHREAD(ram, clk.pos());
    reset_signal_is(rst, true);
  }

};

#endif
