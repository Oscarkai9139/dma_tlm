#include "dma.h"
#include "cpu.h"
#include "ram.h"

int sc_main(int argc, char* argv[]){

  sc_signal<bool> ir;
  sc_signal<bool> rst;

  // define clock
  sc_time clk_prd(10, SC_NS);
  sc_clock clk("clock", clk_prd, 0.5, SC_ZERO_TIME, true);

  // Declare CPU, DMA, RAM modules
  CPU *cpu;
  DMA *dma;
  RAM *ram;
  cpu = new CPU("cpu");
  dma = new DMA("dma");
  ram = new RAM("ram");
  // Connect Modules
  //  ____________  ___________________  ___________
  //  |   CPU    |  |       DMA       |  |   RAM   |
  //  |__Master__|  |__Slave__Master__|  |__Slave__|
  //       |_____________|      |_____________|
  //
  cpu->master_port.bind(dma->slave_port);
  dma->master_port.bind(ram->slave_port);

  cpu->rst(rst);
  cpu->clk(clk);
  dma->rst(rst);
  dma->clk(clk);
  dma->ir(ir);
  ram->rst(rst);
  ram->clk(clk);

  // Set wave signal name
  sc_trace_file *tf = sc_create_vcd_trace_file("RESULT");
  sc_trace(tf, clk, "clock");
  sc_trace(tf, rst, "reset");
  sc_trace(tf, ir,  "interupt");
  sc_trace(tf, dma->R_SOURCE, "SOURCE");
  sc_trace(tf, dma->R_TARGET, "TARGET");
  sc_trace(tf, dma->R_SIZE,   "SIZE");
  sc_trace(tf, dma->R_START,  "START");
  sc_trace(tf, dma->s_addr_temp, "s_addr_temp");
  sc_trace(tf, dma->t_addr_temp, "t_addr_temp");
  sc_trace(tf, dma->data_m,      "data_m");
  sc_trace(tf, dma->rw_m,        "rw_m");

  // Start simulation
  //------------------------------
  sc_start(0, SC_NS);
  rst.write(0);
  sc_start(clk_prd);
  rst.write(1);
  sc_start(clk_prd);
  rst.write(0);

  sc_start(clk_prd*100);
  sc_close_vcd_trace_file(tf);
  //-------------------------------

  return(0);
}
