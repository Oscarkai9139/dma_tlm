#include "cpu.h"
#include "register_define.h"

void CPU::cpu(){

  initial_setup = 0;
  data_temp     = 0;
  addr_temp     = 0;

  while(1){
    wait();

    tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;
    tlm::tlm_command cmd;
    sc_time delay = sc_time(10, SC_NS);

    if(!rst.read()){
      if(initial_setup == 0){
        //Set SOURCE register
        addr_temp = SOURCE_ADDR;
        data_temp = source_init;

        cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_temp));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        initial_setup++;
      }
      else if(initial_setup == 1){
        //Set TARGET register
        addr_temp = TARGET_ADDR;
        data_temp = target_init;

        cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_temp));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        initial_setup++;
      }
      else if(initial_setup == 2){
        //Set SIZE register
        addr_temp = SIZE_ADDR;
        data_temp = size_init;

        cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_temp));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        initial_setup++;
      }
      else if(initial_setup == 3){
        //Set START register
        addr_temp = START_ADDR;
        data_temp = DMA_START;

        cmd = tlm::TLM_WRITE_COMMAND;
        trans->set_command(cmd);
        trans->set_address(addr_temp);
        trans->set_data_ptr(reinterpret_cast<unsigned char*> (&data_temp));
        trans->set_data_length(4);
        trans->set_streaming_width(4);
        trans->set_byte_enable_ptr(0);
        trans->set_dmi_allowed(false);
        trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
        // Sent to DMA slave port
        master_port->b_transport(*trans, delay);
        initial_setup++;
      }
    }

  }//close while loop
}//clsoe dma()
