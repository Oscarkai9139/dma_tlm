#include "ram.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

void RAM::b_transport( tlm::tlm_generic_payload& trans, sc_time& delay ){

  tlm::tlm_command cmd_s  = trans.get_command();
  sc_dt::uint64    addr_s = trans.get_address();
  unsigned char*   data_s = trans.get_data_ptr();
  unsigned int     len    = trans.get_data_length();

  if(cmd_s == tlm::TLM_READ_COMMAND) {
    data_out = RMEM[addr_s];
    *data_s = data_out;
    //cout << "data out: " << data_out << endl;
  }
  else {
    RMEM[addr_s] = *(reinterpret_cast<unsigned char*>(data_s));
    //cout << "data in:  " << RMEM[addr_s] << endl;
  }

  wait(delay);
  trans.set_response_status( tlm::TLM_OK_RESPONSE );
}

void RAM::ram(){

  // Give random number in memory
  for(int i = 0; i < MEM_SIZE; i++) RMEM[i] = (unsigned)0x1 + i*5;

  while(1){
    wait();
  }
}
